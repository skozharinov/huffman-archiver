﻿#ifndef BINARYHEAP_HPP
#define BINARYHEAP_HPP

#include <functional>

template <class Type, class Comparator = std::less<Type>> class BinaryHeap {
public:
  BinaryHeap();
  BinaryHeap(const BinaryHeap<Type, Comparator> &);
  ~BinaryHeap();

  void push(const Type &);
  void pushPtr(Type *);
  const Type &top() const;
  Type pop();
  Type *popPtr();
  bool empty() const;
  unsigned long size() const;
  unsigned long capacity() const;

  BinaryHeap<Type, Comparator> &operator=(const BinaryHeap<Type, Comparator> &);

protected:
  void bubbleUp(unsigned long);
  void sinkDown(unsigned long);
  void swap(unsigned long, unsigned long);
  void checkSize();
  void resizeUp();
  void resizeDown();
  bool compare(unsigned long, unsigned long);

private:
  Type **array;
  unsigned long _size;
  unsigned long _capacity;
  Comparator comparator;
};

template <class Type, class Comparator>
BinaryHeap<Type, Comparator>::BinaryHeap() {
  _capacity = 1;
  _size = 0;
  array = new Type *[_capacity + 1];
  comparator = Comparator();
}

template <class Type, class Comparator>
BinaryHeap<Type, Comparator>::BinaryHeap(const BinaryHeap &heap) {
  _capacity = heap.capacity;
  _size = heap.size;
  array = new Type *[_capacity + 1];
  comparator = Comparator();

  for (unsigned long i = 1; i <= heap.size; i++) {
    array[i] = new Type(*heap.array[i]);
  }
}

template <class Type, class Comparator>
BinaryHeap<Type, Comparator>::~BinaryHeap() {
  for (unsigned long i = 1; i <= _size; i++) {
    delete array[i];
  }

  delete[] array;
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::push(const Type &value) {
  pushPtr(new Type(value));
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::pushPtr(Type *ptr) {
  _size++;
  checkSize();
  array[_size] = ptr;
  bubbleUp(_size);
}

template <class Type, class Comparator>
const Type &BinaryHeap<Type, Comparator>::top() const {
  return *array[1];
}

template <class Type, class Comparator>
Type BinaryHeap<Type, Comparator>::pop() {
  Type *ptr = popPtr();
  Type result = *ptr;
  delete ptr;
  return result;
}

template <class Type, class Comparator>
Type *BinaryHeap<Type, Comparator>::popPtr() {
  Type *result = array[1];
  array[1] = array[_size];
  array[_size] = nullptr;
  _size--;
  checkSize();
  sinkDown(1);
  return result;
}

template <class Type, class Comparator>
bool BinaryHeap<Type, Comparator>::empty() const {
  return _size == 0;
}

template <class Type, class Comparator>
unsigned long BinaryHeap<Type, Comparator>::size() const {
  return _size;
}

template <class Type, class Comparator>
unsigned long BinaryHeap<Type, Comparator>::capacity() const {
  return _capacity;
}

template <class Type, class Comparator>
BinaryHeap<Type, Comparator> &BinaryHeap<Type, Comparator>::operator=(
    const BinaryHeap<Type, Comparator> &heap) {
  for (unsigned long i = 1; i <= _size; i++) {
    delete array[i];
  }

  _size = heap.size;

  if (heap.capacity != _capacity) {
    delete[] array;
    _capacity = heap.capacity;
    array = new Type *[_capacity + 1];
  }

  for (unsigned long i = 1; i <= heap.size; i++) {
    array[i] = new Type(*heap.array[i]);
  }
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::bubbleUp(unsigned long index) {
  while (index > 1 && !compare(index / 2, index)) {
    swap(index / 2, index);
    index /= 2;
  }
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::sinkDown(unsigned long index) {
  if (index <= _size &&
      (!compare(index, index * 2) || !compare(index, index * 2 + 1))) {
    if (compare(index * 2, index * 2 + 1)) {
      swap(index, index * 2);
      sinkDown(index * 2);
    } else {
      swap(index, index * 2 + 1);
      sinkDown(index * 2 + 1);
    }
  }
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::swap(unsigned long first,
                                        unsigned long second) {
  Type *temp = array[first];
  array[first] = array[second];
  array[second] = temp;
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::checkSize() {
  if (_size > _capacity) {
    resizeUp();
  } else if (_size < _capacity / 2) {
    resizeDown();
  }
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::resizeUp() {
  _capacity *= 2;
  Type **newArray = new Type *[_capacity + 1];

  for (unsigned long i = 1; i <= _size; i++) {
    newArray[i] = array[i];
  }

  delete[] array;
  array = newArray;
}

template <class Type, class Comparator>
void BinaryHeap<Type, Comparator>::resizeDown() {
  _capacity /= 2;
  Type **newArray = new Type *[_capacity + 1];

  for (unsigned long i = 1; i <= _size; i++) {
    newArray[i] = array[i];
  }

  delete[] array;
  array = newArray;
}

template <class Type, class Comparator>
bool BinaryHeap<Type, Comparator>::compare(unsigned long first,
                                           unsigned long second) {
  if (first == 0 || first > _size) {
    return false;
  } else if (second == 0 || second > _size) {
    return true;
  } else {
    return comparator(*array[first], *array[second]);
  }
}

#endif // BINARYHEAP_HPP