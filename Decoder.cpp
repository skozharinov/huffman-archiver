﻿#include "Decoder.hpp"
#include "HuffmanTable.hpp"
#include "HuffmanTree.hpp"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <vector>

Decoder::Decoder(const std::string &filename) : inputFilename(filename) {}

void Decoder::decode() {
  std::filesystem::path inputPath(inputFilename);
  std::filesystem::path outputPath(inputPath.parent_path());

  std::string inputString = readFile(inputPath);

  unsigned long long it = 0;

  unsigned char nameLength = static_cast<unsigned char>(inputString.at(it++));
  std::string name = inputString.substr(it, nameLength);
  outputPath.append(name);
  it += nameLength;

  unsigned int chars = static_cast<unsigned char>(inputString.at(it++));
  chars = (chars << 8) | static_cast<unsigned char>(inputString.at(it++));

  HuffmanTable codes;

  for (unsigned int i = 0; i < chars; i++) {
    char ch = inputString.at(it++);
    unsigned char length = static_cast<unsigned char>(inputString.at(it++));
    std::vector<char> code;

    for (unsigned char j = 0; j < (length + 7) / 8; j++) {
      code.push_back(inputString.at(it++));
    }

    codes.addCode(ch, HuffmanTable::unpack(code, length));
  }

  unsigned long long bits = static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));
  bits = (bits << 8) | static_cast<unsigned char>(inputString.at(it++));

  std::vector<char> encoded;
  for (unsigned long long i = 0; i < (bits + 7) / 8; i++) {
    encoded.push_back(inputString.at(it++));
  }

  std::vector<bool> data = HuffmanTable::unpack(encoded, bits);
  HuffmanTree tree = codes.toHuffmanTree();
  std::string result = tree.decode(data);
  std::ofstream output(outputPath);
  output << result;
}

std::string Decoder::readFile(const std::string &filename) {
  std::ifstream ifs(filename.c_str(),
                    std::ios::in | std::ios::binary | std::ios::ate);

  unsigned long fileSize = static_cast<unsigned long>(ifs.tellg());
  ifs.seekg(0, std::ios::beg);

  std::vector<char> bytes(fileSize);
  ifs.read(bytes.data(), static_cast<long>(fileSize));

  return std::string(bytes.data(), fileSize);
}
