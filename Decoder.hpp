﻿#ifndef DECODER_HPP
#define DECODER_HPP

#include <string>

class Decoder {
public:
  Decoder(const std::string &);
  void decode();

private:
  static std::string readFile(const std::string &);
  std::string inputFilename;
};

#endif // DECODER_HPP
