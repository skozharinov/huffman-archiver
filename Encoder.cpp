﻿#include "Encoder.hpp"
#include "HuffmanTable.hpp"
#include "HuffmanTree.hpp"
#include <filesystem>
#include <fstream>
#include <vector>

Encoder::Encoder(const std::string &filename) : inputFilename(filename) {}

void Encoder::encode() {
  std::filesystem::path inputPath(inputFilename);
  std::ofstream output(getOutputFilename(inputFilename));

  std::string savedName = inputPath.filename();

  output << static_cast<unsigned char>(savedName.length());
  output << savedName;

  std::string inputString = readFile(inputPath);

  HuffmanTree tree(inputString);
  HuffmanTable codes(tree);

  unsigned long chars = codes.getCharsCount();
  output << static_cast<unsigned char>(chars >> 8);
  output << static_cast<unsigned char>(chars);

  for (char ch : codes.getChars()) {
    output << ch;
    output << static_cast<unsigned char>(codes.getCodeLength(ch));

    for (char byte : HuffmanTable::pack(codes.getCode(ch))) {
      output << byte;
    }
  }

  std::vector<bool> encoded = codes.encode(inputString);

  unsigned long long bits = encoded.size();
  output << static_cast<unsigned char>(bits >> 56);
  output << static_cast<unsigned char>(bits >> 48);
  output << static_cast<unsigned char>(bits >> 40);
  output << static_cast<unsigned char>(bits >> 32);
  output << static_cast<unsigned char>(bits >> 24);
  output << static_cast<unsigned char>(bits >> 16);
  output << static_cast<unsigned char>(bits >> 8);
  output << static_cast<unsigned char>(bits);

  for (char byte : HuffmanTable::pack(encoded)) {
    output << byte;
  }
}

std::string Encoder::readFile(const std::string &filename) {
  std::ifstream ifs(filename.c_str(),
                    std::ios::in | std::ios::binary | std::ios::ate);

  unsigned long fileSize = static_cast<unsigned long>(ifs.tellg());
  ifs.seekg(0, std::ios::beg);

  std::vector<char> bytes(fileSize);
  ifs.read(bytes.data(), static_cast<long>(fileSize));

  return std::string(bytes.data(), fileSize);
}

std::string Encoder::getOutputFilename(const std::string &filename) {
  std::filesystem::path inputPath(filename);
  std::filesystem::path outputPath = inputPath.parent_path();
  outputPath.append(std::string(inputPath.stem()) + ".xxx");
  return outputPath;
}
