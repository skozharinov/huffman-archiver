﻿#ifndef ENCODER_HPP
#define ENCODER_HPP

#include <string>

class Encoder {
public:
  Encoder(const std::string &);
  void encode();
  static std::string getOutputFilename(const std::string &);

private:
  static std::string readFile(const std::string &);
  std::string inputFilename;
};

#endif // ENCODER_HPP
