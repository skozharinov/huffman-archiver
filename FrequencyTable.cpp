﻿#include "FrequencyTable.hpp"

FrequencyTable::FrequencyTable(const std::string &str) {
  for (char ch : str) {
    frequencies[ch]++;
  }
}

unsigned long FrequencyTable::getFrequency(char ch) const {
  auto it = frequencies.find(ch);

  if (it == frequencies.end()) {
    return 0;
  } else {
    return it->second;
  }
}

unsigned char FrequencyTable::getCharsCount() const {
  return static_cast<unsigned char>(frequencies.size());
}

std::vector<char> FrequencyTable::getChars() const {
  std::vector<char> chars;

  for (auto pair : frequencies) {
    chars.push_back(pair.first);
  }

  return chars;
}
