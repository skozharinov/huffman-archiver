﻿#ifndef FREQUENCYTABLE_HPP
#define FREQUENCYTABLE_HPP

#include <string>
#include <unordered_map>
#include <vector>

class FrequencyTable {
public:
  FrequencyTable(const std::string &);
  unsigned long getFrequency(char) const;
  unsigned char getCharsCount() const;
  std::vector<char> getChars() const;

private:
  std::unordered_map<char, unsigned long> frequencies;
};

#endif // FREQUENCYTABLE_HPP
