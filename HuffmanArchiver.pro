QT += core gui widgets
TARGET = HuffmanArchiver
CONFIG += c++17

HEADERS = \
   BinaryHeap.hpp \
   Decoder.hpp \
   Encoder.hpp \
   FrequencyTable.hpp \
   HuffmanNode.hpp \
   HuffmanTable.hpp \
   HuffmanTree.hpp \
   MainWindow.hpp

SOURCES = \
   Decoder.cpp \
   Encoder.cpp \
   FrequencyTable.cpp \
   HuffmanNode.cpp \
   HuffmanTable.cpp \
   HuffmanTree.cpp \
   MainWindow.cpp \
   main.cpp

RESOURCES += \
  resources.qrc

TRANSLATIONS += \
  translation_ru.ts
