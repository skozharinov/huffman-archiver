﻿#include "HuffmanNode.hpp"

HuffmanNode *HuffmanNode::clone(const HuffmanNode *root) {
  if (root == nullptr) {
    return nullptr;
  }

  HuffmanNode *newRoot = new HuffmanNode(root->character, root->frequency);
  newRoot->left = clone(root->left);
  newRoot->right = clone(root->right);
  return newRoot;
}

void HuffmanNode::clear(HuffmanNode *root) {
  if (root != nullptr) {
    clear(root->left);
    clear(root->right);
    delete root;
  }
}

HuffmanNode::HuffmanNode(char ch, unsigned long freq) {
  character = ch;
  frequency = freq;
  left = nullptr;
  right = nullptr;
  terminal = false;
}

bool HuffmanNode::operator<(const HuffmanNode &node) const {
  return frequency < node.frequency;
}
