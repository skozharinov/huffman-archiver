﻿#ifndef HUFFMANNODE_HPP
#define HUFFMANNODE_HPP

struct HuffmanNode {
  static HuffmanNode *clone(const HuffmanNode *);
  static void clear(HuffmanNode *);

  HuffmanNode *left, *right;
  unsigned long frequency;
  char character;
  bool terminal;

  HuffmanNode(char, unsigned long = 0);

  bool operator<(const HuffmanNode &) const;
};

#endif // HUFFMANNODE_HPP
