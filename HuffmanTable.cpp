﻿#include "HuffmanTable.hpp"
#include "HuffmanNode.hpp"
#include "HuffmanTree.hpp"

HuffmanTable::HuffmanTable(const HuffmanTree &tree) {
  std::vector<bool> code;

  if (tree.root == nullptr) {
    return;
  }

  if (tree.root->left == nullptr && tree.root->right == nullptr) {
    code.push_back(0);
  }

  helper(tree.root, code);
}

void HuffmanTable::addCode(char ch, const std::vector<bool> &code) {
  codes[ch] = code;
}

std::vector<bool> HuffmanTable::getCode(char ch) const { return codes.at(ch); }

unsigned long HuffmanTable::getCodeLength(char ch) const {
  return static_cast<unsigned char>(codes.at(ch).size());
}

unsigned long HuffmanTable::getCharsCount() const {
  return static_cast<unsigned long long>(codes.size());
}

std::vector<char> HuffmanTable::getChars() const {
  std::vector<char> chars;

  for (auto pair : codes) {
    chars.push_back(pair.first);
  }

  return chars;
}

HuffmanTree HuffmanTable::toHuffmanTree() const {
  HuffmanNode *root = new HuffmanNode('\0', 0);

  for (std::pair<char, std::vector<bool>> pair : codes) {
    char symbol = pair.first;
    std::vector<bool> code = pair.second;
    HuffmanNode *node = root;
    for (unsigned char i = 0; i < code.size(); i++) {
      if (code[i]) {
        if (node->right == nullptr) {
          node->right = new HuffmanNode('\0', 0);
        }

        node = node->right;

        if (i == code.size() - 1) {
          node->character = symbol;
          node->terminal = true;
        }
      } else {
        if (node->left == nullptr) {
          node->left = new HuffmanNode('\0', 0);
        }

        node = node->left;

        if (i == code.size() - 1) {
          node->character = symbol;
          node->terminal = true;
        }
      }
    }
  }

  return HuffmanTree{root};
}

std::vector<bool> HuffmanTable::encode(const std::string &str) {
  std::vector<bool> result;

  for (char ch : str) {
    std::vector<bool> code = codes.at(ch);

    for (bool bit : code) {
      result.push_back(bit);
    }
  }

  return result;
}

std::vector<char> HuffmanTable::pack(const std::vector<bool> &unpacked) {
  std::vector<char> packed;
  char temp = 0, count = 0;

  for (bool bit : unpacked) {
    if (count == 8) {
      packed.push_back(temp);
      temp = 0;
      count = 0;
    }

    temp <<= 1;
    count++;

    if (bit) {
      temp |= 1;
    }
  }

  temp <<= 8 - count;
  packed.push_back(temp);
  return packed;
}

std::vector<bool> HuffmanTable::unpack(const std::vector<char> &packed,
                                       unsigned long long length) {
  unsigned long it = 0;
  std::vector<bool> unpacked;

  for (unsigned long long j = 0; j < (length + 7) / 8; j++) {
    for (int i = 7; i >= 0; i--) {
      unpacked.push_back((packed.at(it) >> i) & 1);
    }

    it++;
  }

  unpacked.resize(length);
  return unpacked;
}

void HuffmanTable::helper(HuffmanNode *root, std::vector<bool> code) {
  if (root->left == nullptr && root->left == nullptr) {
    codes[root->character] = code;
  } else {
    if (root->left != nullptr) {
      std::vector<bool> newCode = code;
      newCode.push_back(0);
      helper(root->left, newCode);
    }

    if (root->right != nullptr) {
      std::vector<bool> newCode = code;
      newCode.push_back(1);
      helper(root->right, newCode);
    }
  }
}
