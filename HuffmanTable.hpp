﻿#ifndef HUFFMANTABLE_HPP
#define HUFFMANTABLE_HPP

#include <unordered_map>
#include <vector>
#include <string>

class HuffmanTree;
class HuffmanNode;

class HuffmanTable {
public:
  HuffmanTable() = default;
  HuffmanTable(const HuffmanTree &);
  void addCode(char, const std::vector<bool> &);
  std::vector<bool> getCode(char) const;
  unsigned long getCodeLength(char) const;
  unsigned long getCharsCount() const;
  std::vector<char> getChars() const;
  HuffmanTree toHuffmanTree() const;
  std::vector<bool> encode(const std::string &);
  static std::vector<char> pack(const std::vector<bool> &);
  static std::vector<bool> unpack(const std::vector<char> &,
                                  unsigned long long);

protected:
  void helper(HuffmanNode *, std::vector<bool>);

private:
  std::unordered_map<char, std::vector<bool>> codes;
};

#endif // HUFFMANTABLE_HPP
