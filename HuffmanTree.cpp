﻿#include "HuffmanTree.hpp"
#include "BinaryHeap.hpp"
#include "FrequencyTable.hpp"
#include "HuffmanNode.hpp"

HuffmanTree::HuffmanTree(const std::string &str) {
  FrequencyTable frequencies(str);
  BinaryHeap<HuffmanNode> heap;

  for (char ch : frequencies.getChars()) {
    HuffmanNode *node = new HuffmanNode(ch, frequencies.getFrequency(ch));
    node->terminal = true;
    heap.pushPtr(node);
  }

  while (heap.size() > 1) {
    HuffmanNode *left = heap.popPtr();
    HuffmanNode *right = heap.popPtr();
    unsigned long frequency = left->frequency + right->frequency;
    HuffmanNode *top = new HuffmanNode('\0', frequency);
    top->left = left;
    top->right = right;
    heap.pushPtr(top);
  }

  if (heap.empty()) {
    root = nullptr;
  } else {
    root = heap.popPtr();
  }
}

HuffmanTree::HuffmanTree(const HuffmanTree &tree) {
  root = HuffmanNode::clone(tree.root);
}

HuffmanTree::~HuffmanTree() { HuffmanNode::clear(root); }

std::string HuffmanTree::decode(const std::vector<bool> &data) {
  std::string string;
  HuffmanNode *node = root;

  for (bool bit : data) {
    if (bit) {
      node = node->right;
    } else {
      node = node->left;
    }

    if (node->terminal) {
      string += node->character;
      node = root;
    }
  }

  return string;
}

HuffmanTree::HuffmanTree(HuffmanNode *newRoot) { root = newRoot; }
