﻿#ifndef HUFFMANTREE_HPP
#define HUFFMANTREE_HPP

#include <map>
#include <string>
#include <vector>

class HuffmanNode;

class HuffmanTree {
  friend class HuffmanTable;

public:
  HuffmanTree(const std::string &);
  HuffmanTree(const HuffmanTree &);
  ~HuffmanTree();

  std::string decode(const std::vector<bool> &);

private:
  HuffmanTree(HuffmanNode *);
  HuffmanNode *root;
};

#endif // HUFFMANTREE_HPP
