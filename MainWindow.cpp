﻿#include "MainWindow.hpp"
#include "Decoder.hpp"
#include "Encoder.hpp"
#include <QFileDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QWidget>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
  setWindowTitle(tr("Huffman Archiver"));
  setWindowIcon(QIcon("://icon.png"));
  QWidget *centralWidget = new QWidget();

  QGroupBox *encodeBox = new QGroupBox(tr("Encode"));

  encodeInputLabel = new QLabel(tr("File is not selected"));
  encodeOutputLabel = new QLabel();

  loadEncodeButton = new QPushButton(tr("File..."));
  connect(loadEncodeButton, &QPushButton::clicked, this,
          &MainWindow::loadEncode);

  encodeButton = new QPushButton(tr("Encode"));
  encodeButton->setEnabled(false);
  connect(encodeButton, &QPushButton::clicked, this, &MainWindow::encode);

  QGridLayout *encodeBoxLayout = new QGridLayout();
  encodeBoxLayout->addWidget(encodeInputLabel, 0, 0, 1, 4);
  encodeBoxLayout->addWidget(encodeOutputLabel, 1, 0, 1, 4);
  encodeBoxLayout->addWidget(loadEncodeButton, 0, 4, 1, 1);
  encodeBoxLayout->addWidget(encodeButton, 1, 4, 1, 1);

  encodeBox->setLayout(encodeBoxLayout);

  QGroupBox *decodeBox = new QGroupBox(tr("Decode"));

  decodeInputLabel = new QLabel(tr("File is not selected"));
  decodeOutputLabel = new QLabel();

  loadDecodeButton = new QPushButton(tr("File..."));
  connect(loadDecodeButton, &QPushButton::clicked, this,
          &MainWindow::loadDecode);

  decodeButton = new QPushButton(tr("Decode"));
  decodeButton->setEnabled(false);
  connect(decodeButton, &QPushButton::clicked, this, &MainWindow::decode);

  QGridLayout *decodeBoxLayout = new QGridLayout();
  decodeBoxLayout->addWidget(decodeInputLabel, 0, 0, 1, 4);
  decodeBoxLayout->addWidget(decodeOutputLabel, 1, 0, 1, 4);
  decodeBoxLayout->addWidget(loadDecodeButton, 0, 4, 1, 1);
  decodeBoxLayout->addWidget(decodeButton, 1, 4, 1, 1);

  decodeBox->setLayout(decodeBoxLayout);

  QVBoxLayout *layout = new QVBoxLayout();
  layout->addWidget(encodeBox);
  layout->addWidget(decodeBox);
  centralWidget->setLayout(layout);
  centralWidget->setMinimumSize(500, 200);

  setCentralWidget(centralWidget);
}

void MainWindow::loadEncode() {
  encodeFilename = QFileDialog::getOpenFileName(
      this, tr("Select file to encode"), "", tr("All Files (*)"));

  if (encodeFilename.isEmpty() || encodeFilename.isNull()) {
    encodeInputLabel->setText(tr("File is not selected"));
    encodeOutputLabel->clear();
    encodeButton->setEnabled(false);
  } else {
    encodeInputLabel->setText(tr("Selected: %1").arg(encodeFilename));
    encodeOutputLabel->setText(
        tr("Encoded file: %1")
            .arg(QString::fromStdString(
                Encoder::getOutputFilename(encodeFilename.toStdString()))));
    encodeButton->setEnabled(true);
  }
}

void MainWindow::loadDecode() {
  decodeFilename = QFileDialog::getOpenFileName(
      this, tr("Select file to decode"), "",
      tr("Compressed file (*.xxx);;All Files (*)"));

  if (decodeFilename.isEmpty() || decodeFilename.isNull()) {
    decodeInputLabel->setText(tr("File is not selected"));
    decodeOutputLabel->clear();
    decodeButton->setEnabled(false);
  } else {
    decodeInputLabel->setText(tr("Selected: %1").arg(decodeFilename));
    decodeOutputLabel->setText(tr("Decoded file will have it's original name"));
    decodeButton->setEnabled(true);
  }
}

void MainWindow::encode() {
  try {
    Encoder(encodeFilename.toStdString()).encode();
  } catch (std::exception &e) {
    qDebug("%s", e.what());
  }

  encodeInputLabel->setText(tr("File is not selected"));
  encodeOutputLabel->clear();
  encodeButton->setEnabled(false);
}

void MainWindow::decode() {
  try {
    Decoder(decodeFilename.toStdString()).decode();
  } catch (std::exception &e) {
    qDebug("%s", e.what());
  }

  decodeInputLabel->setText(tr("File is not selected"));
  decodeOutputLabel->clear();
  decodeButton->setEnabled(false);
}
