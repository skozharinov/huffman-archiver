﻿#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QString>

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow(QWidget * = nullptr);

public slots:
  void loadEncode();
  void loadDecode();
  void encode();
  void decode();

private:
  QString encodeFilename;
  QString decodeFilename;
  QPushButton *loadEncodeButton;
  QPushButton *loadDecodeButton;
  QPushButton *encodeButton;
  QPushButton *decodeButton;
  QLabel *encodeInputLabel;
  QLabel *encodeOutputLabel;
  QLabel *decodeInputLabel;
  QLabel *decodeOutputLabel;
};

#endif // MAINWINDOW_HPP
