# Huffman Archiver
Huffman Archiver compresses files using Huffman alghorithm. It is best suited
for text files.

### Compilation
```bash
qmake HuffmanArchiver.pro
make
```

### Usage
```bash
./HuffmanArchiver
```

To compress your file select it in 'Encode' section and press the 'Encode'
button. Path to the compressed .xxx file will be displayed under the original
file path.

To decompress an .xxx file select it in 'Decode' section and press the 'Decode'
button. The file will have it's original name and will be placed in the same
directory with the .xxx file.

### .xxx File Structure
1. 1 byte for the length of the original file's name - **S**.
2. **S** bytes for the original file's name.
3. 2 bytes for the number of symbols in Huffman tree - **X**.
4. **X** blocks describing the tree in the following format:
   1. 1 byte for the character.
   2. 1 byte for the length of the character's code - **T**.
   3. **T** bits for the character's code padded with zeros at the right to a multiple of 8.
5. 8 bytes for the length of the encoded message in bits - **L**.
6. **L** bits for the encoded message padded with zeros at the right to a multiple of 8.

### Authors of the .xxx format
* [skozharinov](https://gitlab.com/skozharinov)
* [exsandebest](https://github.com/exsandebest)
* [klimkomx](https://github.com/klimkomx)
