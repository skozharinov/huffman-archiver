﻿#include "MainWindow.hpp"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  QTranslator translator;
  if (translator.load(QLocale(), "translation", "_", "://")) {
    app.installTranslator(&translator);
  }

  MainWindow window;
  window.show();

  return app.exec();
}
