<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.cpp" line="11"/>
        <source>Huffman Archiver</source>
        <translation>Huffman Archiver</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="15"/>
        <location filename="MainWindow.cpp" line="24"/>
        <source>Encode</source>
        <translation>Кодировать</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="17"/>
        <location filename="MainWindow.cpp" line="38"/>
        <location filename="MainWindow.cpp" line="71"/>
        <location filename="MainWindow.cpp" line="90"/>
        <location filename="MainWindow.cpp" line="102"/>
        <location filename="MainWindow.cpp" line="109"/>
        <source>File is not selected</source>
        <translation>Файл не выбран</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="20"/>
        <location filename="MainWindow.cpp" line="41"/>
        <source>File...</source>
        <translation>Файл...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="36"/>
        <location filename="MainWindow.cpp" line="45"/>
        <source>Decode</source>
        <translation>Декодировать</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="68"/>
        <source>Select file to encode</source>
        <translation>Выберите файл для кодирования</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="68"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="75"/>
        <location filename="MainWindow.cpp" line="94"/>
        <source>Selected: %1</source>
        <translation>Выбранный файл: %1</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="77"/>
        <source>Encoded file: %1</source>
        <translation>Закодированный файл: %1</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="86"/>
        <source>Select file to decode</source>
        <translation>Выберите файл для декодирования</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="87"/>
        <source>Compressed file (*.xxx);;All Files (*)</source>
        <translation>Сжатый файл (*.xxx);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="95"/>
        <source>Decoded file will have it&apos;s original name</source>
        <translation>Раскодированный файл получит своё исходное имя</translation>
    </message>
</context>
</TS>
